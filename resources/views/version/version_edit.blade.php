@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('تحديث  ')}}</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="{{route('version.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الاسم') }}</label>
                                        <input type="text" class="form-control" placeholder="الاسم"
                                               name="link" value="{{$versions->link}}">
                                        <input type="hidden" name="id" value="{{$versions->id}}">

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('رقم الهاتف') }}</label>
                                        <input type="number" class="form-control"
                                               name="ver" value="{{$versions->ver}}">
                                    </div>
                                </div>

                                <div class="form-actions mt-10">
                                    <button type="submit" class="btn btn-primary"><i
                                            class="fa fa-check"></i> {{__('تحديث') }}</button>
                                    <a href="{{route('version.index')}}" class="btn btn-danger">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>

@endsection

