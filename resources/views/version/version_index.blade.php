@extends('layouts.dashboard')
@section('content')
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border p-3" style="margin-bottom:15px">
                    <h4 class="box-title text-info my-0"><i
                            class="mdi mdi-account-multiple me-15"></i>{{__('قائمة التحديثات')}}</h4>
                </div>
                <div class="mx-5">
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <a href="{{ route('version.create') }}" class="btn btn-success mx-5">{{__('إضافة رابط')}}</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="box">
                        <div class="box-body p-0">
                            <div class="table-responsive rounded card-table">
                                <table class="table border-no" id="example1">
                                    <thead>
                                    <tr>

                                        <th>{{__('الرابط')}}</th>
                                        <th>{{__('رقم الإصدار')}}</th>

                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($versions as $version)
                                            <td>{{$version->link}}</td>
                                            <td>{{$version->ver}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="hover-primary dropdown-toggle no-caret"
                                                       data-bs-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{route('version.edit',$version->id)}}">{{__('تعديل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("version.destroy",$version->id)}}">
                                                                <i class="material-icons"></i> {{__('الحذف')}}
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
