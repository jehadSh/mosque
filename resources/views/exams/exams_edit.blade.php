@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('تحديث طلب التسميع')}}</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="{{route('exam.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div>
                            <div class="row">
                                <input type="hidden" name="id" value="{{$exams->id}}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الطالب : ') }}</label><br>
                                        <label class="fw-700 fs-16 form-label">{{$exams->getStudentNameAttribute()}}</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('رقم الجزء') }}</label>
                                        <input type="number" class="form-control" placeholder="رقم الجزء"
                                               name="shapter_num"
                                               value="{{$exams->shapter_num}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('الحالة') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="status">
                                                <option {{ $exams->status=='null' ? 'selected':''}} value="null">
                                                    لم يتم استلام الطلب
                                                </option>
                                                <option
                                                    {{ $exams->status=='Processing' ? 'selected':''}} value="Processing">
                                                    بانتظار موعد التسميع
                                                </option>
                                                <option
                                                    {{ $exams->status=='Accepted' ? 'selected':''}} value="Accepted">
                                                    نجح صاحب الطلب
                                                </option>
                                                <option
                                                    {{ $exams->status=='Refused' ? 'selected':''}} value="Refused">
                                                    رسب صاحب الطلب
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الملاحظة') }}</label>
                                        <input type="text" class="form-control" placeholder="الملاحظة"
                                               name="note"
                                               value="{{$exams->note}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('التقييم') }}</label>
                                        <input type="number" class="form-control" placeholder="التقييم"
                                               name="rate"
                                               value="{{$exams->rate}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('المسمع') }}</label>
                                        <input type="text" class="form-control" placeholder="المسمع" name="assigned_to"
                                               value="{{$exams->getTeacherNameAttribute()}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('وقت التسميع') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="order_time">
                                                <option
                                                    {{ $exams->order_time=='بعد الفجر' ? 'selected':''}} value="بعد الفجر">
                                                    بعد الفجر
                                                </option>
                                                <option
                                                    {{ $exams->order_time=='بعد الظهر' ? 'selected':''}} value="بعد الظهر">
                                                    بعد الظهر
                                                </option>
                                                <option
                                                    {{ $exams->order_time=='بعد العصر' ? 'selected':''}} value="بعد العصر">
                                                    بعد العصر
                                                </option>
                                                <option
                                                    {{ $exams->order_time=='بين المغرب و العشاء' ? 'selected':''}} value="بين المغرب و العشاء">
                                                    بين المغرب و العشاء
                                                </option>
                                                <option
                                                    {{ $exams->order_time=='بعد العشاء' ? 'selected':''}} value="بعد العشاء">
                                                    بعد العشاء
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('يوم التسميع') }}</label>
                                        <input type="date" class="form-control" placeholder="يوم التسميع"
                                               name="order_date"
                                               value="{{$exams->order_date}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input style="display: none" type="checkbox"
                                               @if($exams->ejaza==1)checked=checked @endif
                                               id="ejaza" name="ejaza" value="1">
                                        <label for="ejaza">إجازة </label><br>

                                    </div>
                                </div>

                            </div>
                            <div class="form-actions mt-10">
                                <button type="submit" class="btn btn-primary"><i
                                        class="fa fa-check"></i> {{__('تحديث') }}</button>
                                <a href="{{route('exam.index')}}" class="btn btn-danger">الغاء</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>
@endsection

