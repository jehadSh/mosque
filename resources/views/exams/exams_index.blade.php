@extends('layouts.dashboard')
@section('content')
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border p-3" style="margin-bottom:15px">
                    <h4 class="box-title text-info my-0"><i
                            class="mdi mdi-account-multiple me-15"></i>{{__('قائمة طلبات تسميع طلاب المسجد')}}</h4>
                </div>

                <div class="col-12">
                    <div class="box">
                        <div class="box-body p-0">
                            <div class="table-responsive rounded card-table">
                                <table class="table border-no" id="example1">
                                    <thead>
                                    <tr>

                                        <th>{{__('الطالب')}}</th>
                                        <th>{{__('الجزء')}}</th>
<!--                                        <th>{{__('إجازة')}}</th>-->
                                        <th>{{__('حالة الطلب')}}</th>
                                        <th>{{__('المسمع')}}</th>
                                        <th>{{__('وقت التسميع')}}</th>
                                        <th>{{__('تاريخ التسميع')}}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($exams as $exam)
                                        <tr @if($exam->deleted_at) class="deleted" @endif class="hover-primary">
                                            <td>
                                                {{$exam->getStudentNameAttribute()}}
                                            </td>
                                            <td>{{$exam->shapter_num}}</td>
<!--                                            <td>
                                                @if($exam->ejaza==1)
                                                    طلب إجازة
                                                @else
                                                    ليس طلب إجازة
                                                @endif
                                            </td>-->
                                            <td>
                                                @if($exam->status==null)
                                                    لم يتم استلام الطلب
                                                @elseif($exam->status=='Processing')
                                                    تم استلام الطلب
                                                @elseif($exam->status=='Accepted')
                                                    نجح الطلب
                                                @elseif($exam->status=='Refused')
                                                    رسب الطلب
                                                @endif
                                            </td>
                                            <td>
                                                @if($exam->assigned_to==null)
                                                    لا يوجد مسمع بعد
                                                @else
                                                    {{$exam->getTeacherNameAttribute()}}
                                                @endif

                                            </td>
                                            <td>{{$exam->order_time}}</td>

                                            <td>{{$exam->order_date}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="hover-primary dropdown-toggle no-caret"
                                                       data-bs-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{ route("exam.show",$exam->id ) }}">{{__('قراءة التفاصيل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route('exam.edit',$exam->id)}}">{{__('تعديل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("exam.destroy",$exam->id)}}">
                                                            @if(!$exam->deleted_at)
                                                                <i class="material-icons"></i> {{__('الحذف')}}
                                                            @else
                                                                <i class="material-icons"></i> {{__('استعادة')}}
                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
