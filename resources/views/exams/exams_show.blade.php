@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('معلومات طلب التسميع' )}}</h4>
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اسم الطالب')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->getStudentNameAttribute()}}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اسم الأستاذ')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->getTeacherNameAttribute()}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الجزء')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->shapter_num}}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('إجازة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($exams->ejaza==1)
                                        طلب إجازة
                                    @else
                                        ليس طلب إجازة
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الحالة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($exams->status==null)
                                        لم يتم استلام الطلب
                                    @elseif($exams->status=='Processing')
                                        تم استلام الطلب
                                    @elseif($exams->status=='Accepted')
                                        نجح الطلب
                                    @elseif($exams->status=='Refused')
                                        رسب الطلب
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الملاحظة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->note}}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('التقييم')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->rate}}

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الوقت')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->order_time}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اليوم')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$exams->order_date}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
