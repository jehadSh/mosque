@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('تحديث حساب المستخدم')}}</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="{{route('user.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الاسم') }}</label>
                                        <input type="text" class="form-control" placeholder="الاسم"
                                               name="name" value="{{$user_models->name}}">
                                        <input type="hidden" name="id" value="{{$user_models->id}}">
                                        @error('name')
                                        <p class="form-text text-danger">{{$message}} </p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('رقم الهاتف') }}</label>
                                        <input type="tel" class="form-control" placeholder="0-(000)-000-0000"
                                               name="phone" value="{{$user_models->phone}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الصف') }}</label>
                                        <input type="text" class="form-control" placeholder="class" name="class"
                                               value="{{$user_models->class}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الحلقة') }}</label>
                                        <input type="text" class="form-control" placeholder="الحلقة" name="group"
                                               value="{{$user_models->group}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('الدور') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="role">
                                                @if($user_models->role=='admin')
                                                    <option {{ $user_models->role=='admin' ? 'selected':''}} value="admin">
                                                        مشرف
                                                    </option>
                                                @endif
                                                <option
                                                    {{ $user_models->role=='teacher' ? 'selected':''}} value="teacher">
                                                    استاذ حلقة
                                                </option>
                                                <option
                                                    {{ $user_models->role=='Listener' ? 'selected':''}} value="Listener">
                                                    مسمع
                                                </option>
                                                <option
                                                    {{ $user_models->role=='student' ? 'selected':''}} value="student">
                                                    طالب
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('اجازة') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="isEjaza">
                                                <option {{ $user_models->isEjaza=='1' ? 'selected':''}} value="1">
                                                    معه اجازة
                                                </option>
                                                <option {{ $user_models->isEjaza=='0' ? 'selected':''}} value="0">
                                                    ليس لديه اجازة
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('اختبار') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="isExam">
                                                <option {{ $user_models->isExam=='1' ? 'selected':''}} value="1">
                                                    يحق له الاختبار
                                                </option>
                                                <option {{ $user_models->isExam=='0' ? 'selected':''}} value="0">
                                                    لا يحق له الاختبار
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('الحالة') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="activation">
                                                <option {{ $user_models->activation=='1' ? 'selected':''}} value="1">
                                                    مفعل
                                                </option>
                                                <option {{ $user_models->activation=='0' ? 'selected':''}} value="0">
                                                    غير مفعل
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="box">
                                            <div class="box-body">

                                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                        data-bs-target="#modal-center">
                                                    اختيار الأجزاء
                                                </button>
                                            </div>
                                        </div>
                                        <div class="modal center-modal fade" id="modal-center" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content"
                                                     style="height:620px !important;">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">إغلاق</h5>&nbsp;
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body" style="overflow-x: scroll;">
                                                        <div class="" style="display: flex; flex-flow: column nowrap">
                                                            @foreach([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30] as $shapter)
                                                                <div><br></div>
                                                                <input style="display: none" type="checkbox"
                                                                       id="{{$shapter}}" name="{{$shapter}}"
                                                                       value="{{$shapter}}">
                                                                <label for="{{$shapter}}"> الجزء {{$shapter}}</label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer modal-footer-uniform">
                                                        <button type="button" class="btn btn-danger"
                                                                data-bs-dismiss="modal">الغاء
                                                        </button>
                                                        <button onclick="myfunction()"
                                                                type="button" class="btn btn-primary float-end"
                                                                data-bs-dismiss="modal">حفظ
                                                            التغيرات
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="allow_shapter" name="allow_shapter" value="">

                            </div>
                            <div class="form-actions mt-10">
                                <button type="submit" class="btn btn-primary"><i
                                        class="fa fa-check"></i> {{__('تحديث') }}</button>
                                <a href="{{route('user.index')}}" class="btn btn-danger">الغاء</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <script>
        const chapters = [1, 2, 3, 4, 5];

        for (let chapter of {{$user_models->allow_shapter}}) {
            console.log("chapter-", chapter);
            const element = document.getElementById(chapter.toString())
            console.log(element)
            element.checked = true;
            element.value = true;
        }

        function myfunction() {
            let x = []
            for (let i = 1; i <= 30; i++) {
                const element = document.getElementById(i.toString());
                if (element.checked) {
                    x.push(i)
                }
            }
            console.log(x)
            $('#allow_shapter').val('[' + x + ']');
        }
    </script>
@endsection

