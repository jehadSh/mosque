@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('معلومات المستخدم' )}}</h4>
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الاسم')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$user_models->name}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('رقم الهاتف')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$user_models->phone}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الصف')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$user_models->class}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الحلقة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$user_models->group}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الدور')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($user_models->role=='admin')
                                        مشرف
                                    @elseif($user_models->role=='teacher')
                                        استاذ حلقة
                                    @elseif($user_models->role=='Listener')
                                        مسمع
                                    @elseif($user_models->role=='student')
                                        طالب
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الأجزاء المقرئ')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$user_models->allow_shapter}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الإجازة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($user_models->isEjaza==1)
                                        معه اجازة
                                    @else
                                        ليس لديه اجازة
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الاختبار')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($user_models->isExam==1)
                                        يحق له الاختبار
                                    @else
                                        لا يحق له الاختبار
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الحالة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($user_models->activation==1)
                                        مفعل
                                    @else
                                        غير مفعل
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <a class="dropdown-item"
                                   href="{{route("order.getOrderStudent",$user_models->id)}}">
                                    <i class="material-icons"></i> {{__('إظهار جميع طلبات التسميع السابقة')}}
                                </a>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <a class="dropdown-item"
                                   href="{{route("exam.getExamStudent",$user_models->id)}}">
                                    <i class="material-icons"></i> {{__('إظهار جميع طلبات الاختبار السابقة')}}
                                </a>
                            </div>

                            <div class="row">
                                @if($user_models->role != 'student')
                                    <hr>
                                    <a class="dropdown-item"
                                       href="{{route("order.getOrderTeacher",$user_models->id)}}">
                                        <i class="material-icons"></i> {{__('إظهار جميع الطلبات التي قام بتسميعها')}}
                                    </a>
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                    <a class="dropdown-item"
                                       href="{{route("exam.getExamTeacher",$user_models->id)}}">
                                        <i class="material-icons"></i> {{__('إظهار جميع طلبات التي قام باختبارها')}}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
