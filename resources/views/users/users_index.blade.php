@extends('layouts.dashboard')
@section('content')
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border p-3" style="margin-bottom:15px">
                    <h4 class="box-title text-info my-0"><i
                            class="mdi mdi-account-multiple me-15"></i>{{__('قائمة جميع طلاب المسجد')}}</h4>
                </div>
                <div class="mx-5">
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="بحث">
                            <button type="submit" class="btn btn-primary">
                                {{__('بحث')}}
                            </button>
                            <a href="{{ route('user.create') }}" class="btn btn-success mx-5">{{__('إضافة طالب')}}</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="box">
                        <div class="box-body p-0">
                            <div class="table-responsive rounded card-table">
                                <table class="table border-no" id="example1">
                                    <thead>
                                    <tr>
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('الاسم')}}</th>
                                        <th>{{__('رقم الهاتف')}}</th>
                                        <th>{{__('الصف')}}</th>
                                        <th>{{__('الحلقة')}}</th>
                                        <th>{{__('الدور')}}</th>
                                        <th>{{__('الأجزاء المقرئ')}}</th>
                                        <th>{{__('الاختبار')}}</th>
                                        <th>{{__('الحالة')}}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user_models as $user_model)
                                        <tr @if($user_model->deleted_at) class="deleted" @endif class="hover-primary">
                                            <td>{{$user_model->id}}</td>
                                            <td>{{$user_model->name}}</td>
                                            <td>{{$user_model->phone}}</td>
                                            <td>{{$user_model->class}}</td>
                                            <td>{{$user_model->group}}</td>
                                            <td>{{$user_model->role}}</td>
                                            <td>{{$user_model->allow_shapter}}</td>
                                            <td>{{$user_model->isExam}}</td>
                                            <td>{{$user_model->activation}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="hover-primary dropdown-toggle no-caret"
                                                       data-bs-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{ route("user.show",$user_model->id ) }}">{{__('قراءة التفاصيل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route('user.edit',$user_model->id)}}">{{__('تعديل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route('user.getUpdateToAdmin',$user_model->id)}}">{{__('ترقية إلى مشرف')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("user.destroy",$user_model->id)}}">
                                                            @if(!$user_model->deleted_at)
                                                                <i class="material-icons"></i> {{__('الحذف')}}
                                                            @else
                                                                <i class="material-icons"></i> {{__('استعادة')}}
                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
