@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('ترقية الحساب إلى مشرف')}}</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="{{route('user.updateToAdmin')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div>
                            <div class="row">
                                <input type="hidden" name="id" value="{{$user_models->id}}">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الاسم : ') }}</label><br>
                                        <label class="fw-700 fs-16 form-label">{{$user_models->name}}</label>
                                        @error('name')
                                        <p class="form-text text-danger">{{$message}} </p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الإيميل') }}</label>
                                        <input type="email" class="form-control" placeholder="email"
                                               name="email" value="{{$user_models->email}}">
                                        @error('email')
                                        <p class="form-text text-danger">{{$message}} </p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('الدور') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="role">
                                                <option {{ $user_models->role=='admin' ? 'selected':''}} value="admin">
                                                    مشرف
                                                </option>
                                                <option
                                                    {{ $user_models->role=='teacher' ? 'selected':''}} value="teacher">
                                                    استاذ حلقة
                                                </option>
                                                <option
                                                    {{ $user_models->role=='Listener' ? 'selected':''}} value="Listener">
                                                    مسمع
                                                </option>
                                                <option
                                                    {{ $user_models->role=='student' ? 'selected':''}} value="student">
                                                    طالب
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions mt-10">
                                <button type="submit" class="btn btn-primary"><i
                                        class="fa fa-check"></i> {{__('تحديث') }}</button>
                                <a href="{{route('user.index')}}" class="btn btn-danger">الغاء</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>
@endsection

