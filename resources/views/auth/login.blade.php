<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">

    <title>الحفاظ</title>

    <!-- Vendors Style-->
    <link rel="stylesheet" href="{{ asset('assets/css/vendors_css.css') }}">

    <!-- Style-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/skin_color.css') }}">

</head>

<body class="hold-transition theme-primary bg-img" style="background-image: url({{ asset('assets/images/login.jpg') }}">

<div class="container h-p100">
    <div class="row align-items-center justify-content-md-center h-p100">
        <div class="col-12">
            <div class="row justify-content-center g-0">
                <div class="col-lg-5 col-md-5 col-12">
                    <div class="bg-white rounded10 shadow-lg">
                        <div class="content-top-agile p-20 pb-0">
                            <h2 class="text-primary">{{ __('Lets Get Started') }}</h2>
                            <p class="mb-0">{{ __('Sign in to continue to WebkitX') }}</p>.
                        </div>
                        <div class="p-40">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
                                        <input type="email" class="form-control ps-15 bg-transparent" placeholder="email"
                                               name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
                                        <input type="password" class="form-control ps-15 bg-transparent"
                                               placeholder="Password" name="password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="checkbox">
                                            <input type="checkbox" id="basic_checkbox_1">
                                            <label for="basic_checkbox_1">{{ __('Remember Me') }}</label>
                                        </div>
                                    </div>
                                    <!-- /.col -->
<!--                                    <div class="col-6">
                                        <div class="fog-pwd text-end">
                                            <a href="{{ route('password.request') }}" class="hover-warning"><i
                                                    class="ion ion-locked"></i> {{ __('Forgot pwd?') }}</a><br>
                                        </div>
                                    </div>-->
                                    <!-- /.col -->
                                    <button type="submit" class="btn btn-danger mt-10">
                                        {{ __('SIGN IN') }}
                                    </button>

                                    <!-- /.col -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Vendor JS -->
<script src="asset{{ ('assets/js/vendors.min.js') }}"></script>
<script src="asset{{ ('assets/js/pages/chat-popup.js') }}"></script>
<script src="asset{{ ('assets/assets/vendor_components/apexcharts-bundle/dist/apexcharts.min.js') }}"></script>
<script src="asset{{ ('assets/assets/icons/feather-icons/feather.min.js') }}"></script>

</body>

</html>
