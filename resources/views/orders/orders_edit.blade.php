@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('تحديث طلب التسميع')}}</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="POST" action="{{route('order.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div>
                            <div class="row">
                                <input type="hidden" name="id" value="{{$orders->id}}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الطالب : ') }}</label><br>
                                        <label class="fw-700 fs-16 form-label">{{$orders->getStudentNameAttribute()}}</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('رقم الجزء') }}</label>
                                        <input type="number" class="form-control" placeholder="رقم الجزء"
                                               name="shapter_num"
                                               value="{{$orders->shapter_num}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('من الصفحة') }}</label>
                                        <input type="number" class="form-control" placeholder="من الصفحة"
                                               name="from_page"
                                               value="{{$orders->from_page}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('إلى الصفحة') }}</label>
                                        <input type="number" class="form-control" placeholder="إلى الصفحة"
                                               name="to_page"
                                               value="{{$orders->to_page}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('الحالة') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="status">
                                                <option {{ $orders->status=='null' ? 'selected':''}} value="null">
                                                    لم يتم استلام الطلب
                                                </option>
                                                <option
                                                    {{ $orders->status=='Processing' ? 'selected':''}} value="Processing">
                                                    بانتظار موعد التسميع
                                                </option>
                                                <option
                                                    {{ $orders->status=='Accepted' ? 'selected':''}} value="Accepted">
                                                    نجح صاحب الطلب
                                                </option>
                                                <option
                                                    {{ $orders->status=='Refused' ? 'selected':''}} value="Refused">
                                                    رسب صاحب الطلب
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('الملاحظة') }}</label>
                                        <input type="text" class="form-control" placeholder="الملاحظة"
                                               name="note"
                                               value="{{$orders->note}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('التقييم') }}</label>
                                        <input type="number" class="form-control" placeholder="التقييم"
                                               name="rate"
                                               value="{{$orders->rate}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('المسمع') }}</label>
                                        <input type="text" class="form-control" placeholder="المسمع" name="assigned_to"
                                               value="{{$orders->getTeacherNameAttribute()}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('وقت التسميع') }}</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select" name="order_time">
                                                <option
                                                    {{ $orders->order_time=='بعد الفجر' ? 'selected':''}} value="بعد الفجر">
                                                    بعد الفجر
                                                </option>
                                                <option
                                                    {{ $orders->order_time=='بعد الظهر' ? 'selected':''}} value="بعد الظهر">
                                                    بعد الظهر
                                                </option>
                                                <option
                                                    {{ $orders->order_time=='بعد العصر' ? 'selected':''}} value="بعد العصر">
                                                    بعد العصر
                                                </option>
                                                <option
                                                    {{ $orders->order_time=='بين المغرب و العشاء' ? 'selected':''}} value="بين المغرب و العشاء">
                                                    بين المغرب و العشاء
                                                </option>
                                                <option
                                                    {{ $orders->order_time=='بعد العشاء' ? 'selected':''}} value="بعد العشاء">
                                                    بعد العشاء
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="fw-700 fs-16 form-label">{{__('يوم التسميع') }}</label>
                                        <input type="date" class="form-control" placeholder="يوم التسميع"
                                               name="order_date"
                                               value="{{$orders->order_date}}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input style="display: none" type="checkbox"
                                               @if($orders->ejaza==1)checked=checked @endif
                                               id="ejaza" name="ejaza" value="1">
                                        <label for="ejaza">إجازة </label><br>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input style="display: none" type="checkbox"
                                               @if($orders->reading==1)checked=checked @endif
                                               id="reading" name="reading" value="1">
                                        <label for="reading">إقراء </label><br>

                                    </div>
                                </div>

                            </div>
                            <div class="form-actions mt-10">
                                <button type="submit" class="btn btn-primary"><i
                                        class="fa fa-check"></i> {{__('تحديث') }}</button>
                                <a href="{{route('order.index')}}" class="btn btn-danger">الغاء</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>
@endsection

