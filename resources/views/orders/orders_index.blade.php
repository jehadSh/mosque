@extends('layouts.dashboard')
@section('content')
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border p-3" style="margin-bottom:15px">
                    <h4 class="box-title text-info my-0"><i
                            class="mdi mdi-account-multiple me-15"></i>{{__('قائمة طلبات تسميع طلاب المسجد')}}</h4>
                </div>

                <div class="col-12">
                    <div class="box">
                        <div class="box-body p-0">
                            <div class="table-responsive rounded card-table">
                                <table class="table border-no" id="example1">
                                    <thead>
                                    <tr>

                                        <th>{{__('الطالب')}}</th>
                                        <th>{{__('الجزء')}}</th>
                                        <th>{{__('من الصفحة')}}</th>
                                        <th>{{__('إلى الصفحة')}}</th>
                                        <th>{{__('عدد الصفحات')}}</th>
<!--                                        <th>{{__('إجازة')}}</th>-->
                                        <th>{{__('إقراء')}}</th>
                                        <th>{{__('حالة الطلب')}}</th>
                                        <th>{{__('المسمع')}}</th>
                                        <th>{{__('وقت التسميع')}}</th>
                                        <th>{{__('تاريخ التسميع')}}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr @if($order->deleted_at) class="deleted" @endif class="hover-primary">
                                            <td>
                                                {{$order->getStudentNameAttribute()}}
                                            </td>
                                            <td>{{$order->shapter_num}}</td>
                                            <td>{{$order->from_page}}</td>
                                            <td>{{$order->to_page}}</td>
                                            <td>{{$order->count_page}}</td>

                                            <td>
                                                @if($order->reading==1)
                                                    نعم
                                                @else
                                                    لا
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->status==null)
                                                    لم يتم استلام الطلب
                                                @elseif($order->status=='Processing')
                                                    تم استلام الطلب
                                                @elseif($order->status=='Accepted')
                                                    نجح الطلب
                                                @elseif($order->status=='Refused')
                                                    رسب الطلب
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->assigned_to==null)
                                                    لا يوجد مسمع بعد
                                                @else
                                                    {{$order->getTeacherNameAttribute()}}
                                                @endif

                                            </td>
                                            <td>{{$order->order_time}}</td>

                                            <td>{{$order->order_date}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="hover-primary dropdown-toggle no-caret"
                                                       data-bs-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{ route("order.show",$order->id ) }}">{{__('قراءة التفاصيل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route('order.edit',$order->id)}}">{{__('تعديل')}}</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("order.destroy",$order->id)}}">
                                                            @if(!$order->deleted_at)
                                                                <i class="material-icons"></i> {{__('الحذف')}}
                                                            @else
                                                                <i class="material-icons"></i> {{__('استعادة')}}
                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
