@extends('layouts.dashboard')
@section('content')
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title text-info mb-0"><i class="ti-user me-15"></i> {{__('معلومات طلب التسميع' )}}</h4>
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اسم الطالب')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->getStudentNameAttribute()}}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اسم الأستاذ')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->getTeacherNameAttribute()}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الجزء')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->shapter_num}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('من الصفحة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->from_page}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('إلى الصفحة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->to_page}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('إجازة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($orders->ejaza==1)
                                        طلب إجازة
                                    @else
                                        ليس طلب إجازة
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('إقراء')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($orders->reading==1)
                                        طلب إقراء
                                    @else
                                        ليس طلب إقراء
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الحالة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    @if($orders->status==null)
                                        لم يتم استلام الطلب
                                    @elseif($orders->status=='Processing')
                                        تم استلام الطلب
                                    @elseif($orders->status=='Accepted')
                                        نجح الطلب
                                    @elseif($orders->status=='Refused')
                                        رسب الطلب
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الملاحظة')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->note}}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('التقييم')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->rate}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('الوقت')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->order_time}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{__('اليوم')}}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{$orders->order_date}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
