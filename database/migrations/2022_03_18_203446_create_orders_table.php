<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('shapter_num');
            $table->integer('from_page');
            $table->integer('to_page')->nullable();
            $table->integer('count_page')->nullable();
            $table->integer('ejaza')->default(0)->nullable();
            $table->integer('reading')->default(0)->nullable();
            $table->enum('status', ['Accepted', 'Refused', 'Processing'])->nullable();
            $table->longText('note')->nullable();
            $table->integer('rate')->nullable();
            $table->integer('assigned_to')->unsigned()->nullable();
            $table->string('order_time');
            $table->date('order_date');
           // $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assigned_to')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
