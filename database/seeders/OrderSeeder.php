<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'user_id' => '1',
                'shapter_num' => '1',
                'from_page' => '12',
                'to_page' => '16',
                'ejaza' => 0,
                'order_date' => '2021-10-12 00:00:00',
            ],
            [
                'user_id' => '2',
                'shapter_num' => '3',
                'from_page' => '4',
                'to_page' => '8',
                'ejaza' => 1,
                'order_date' => '2021-10-12 00:00:00',
            ],
        ];
        Order::insert($user);
    }
}
