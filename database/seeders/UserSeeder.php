<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'جهاد',
                'email'=>'jehad.shekhzain@gmail.com',
                'password' => Hash::make('aattff123@.'),
                'class' => 'st7',
                'phone' => ('0934151507'),
                'group' => 'faroq',
                'role' => 'admin',
                'allow_shapter' => "[1,2,3,28,29,30]",
                'activation' => '1',
                'isEjaza' => '1',
                'isExam' => '1',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12'
            ]
        ];
        User::insert($user);
    }
}
