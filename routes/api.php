<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ExamController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\TeacherController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("/login", [AuthController::class, 'login']);
Route::post("/register", [AuthController::class, 'register']);

//CRUD For All Table
Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get("isToken", [AuthController::class, 'isToken']);
    Route::post("editPassword", [AuthController::class, 'editPassword']);
    Route::post("/logout", [AuthController::class, 'logout']);

});
// ----------------------------------------Order---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'order'], function () {
    Route::post('store_update', [OrderController::class, 'store_update']);
    Route::post('index', [OrderController::class, 'index']);
    Route::get('show/{id}', [OrderController::class, 'show']);
    Route::delete('delete/{id}', [OrderController::class, 'destroy']);
    Route::get('getMyOrder', [OrderController::class, 'getMyOrder']);
    Route::get('getAllMyOrder', [OrderController::class, 'getAllMyOrder']);
});
// ----------------------------------------Exam---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'exam'], function () {
    Route::post('store_update', [ExamController::class, 'store_update']);
    Route::post('index', [ExamController::class, 'index']);
    Route::get('show/{id}', [ExamController::class, 'show']);
    Route::delete('delete/{id}', [ExamController::class, 'destroy']);
    Route::get('getMyExam', [ExamController::class, 'getMyExam']);
    Route::get('getAllMyExam', [ExamController::class, 'getAllMyExam']);
});
// ----------------------------------------teacher---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'teacher'], function () {
    Route::get('getAllowOrders_Exams', [TeacherController::class, 'getAllowOrders_Exams']);
    Route::post('acceptOrder/{id}', [TeacherController::class, 'acceptOrder']);
    Route::post('acceptExam/{id}', [TeacherController::class, 'acceptExam']);
    Route::post('orderEvaluation', [TeacherController::class, 'orderEvaluation']);
    Route::post('examEvaluation', [TeacherController::class, 'examEvaluation']);
    Route::get('getAllAccepted', [TeacherController::class, 'getAllAccepted']);
    Route::get('getPreviousOrders_Exams', [TeacherController::class, 'getPreviousOrders_Exams']);
    Route::post('cancelAcceptOrder/{id}', [TeacherController::class, 'cancelAcceptOrder']);
    Route::post('cancelAcceptExam/{id}', [TeacherController::class, 'cancelAcceptExam']);
});

Route::post('login_admin', [DashboardController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'dashboard'], function () {

});

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'dashboard/User'], function () {
    Route::get('index', [UserController::class, 'index']);
    Route::post('update', [UserController::class, 'update']);
    Route::get('show/{id}', [UserController::class, 'show']);
    Route::post('destroy/{id}', [UserController::class, 'destroy']);
    Route::get('getAllTeachers', [UserController::class, 'getAllTeachers']);
    Route::post('activateUser', [UserController::class, 'activateUser']);
});
