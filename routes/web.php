<?php

use App\Http\Controllers\Dashboard\ExamController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\UserController;
use App\Http\Controllers\Dashboard\VersionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// ----------------------------------------User---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'user'], function () {
    Route::get('create', [UserController::class, 'create'])->name('user.create');
    Route::post('store', [UserController::class, 'store'])->name('user.store');
    Route::post('update', [UserController::class, 'update'])->name('user.update');
    Route::get('index', [UserController::class, 'index'])->name('user.index');
    Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::get('show/{id}', [UserController::class, 'show'])->name('user.show');
    Route::get('delete/{id}', [UserController::class, 'destroy'])->name('user.destroy');
    Route::get('getAllTeachers', [UserController::class, 'getAllTeachers'])->name('user.getAllTeachers');
    Route::get('getViewActivateUser', [UserController::class, 'getViewActivateUser'])->name('user.getViewActivateUser');
    Route::get('activateUser/{id}', [UserController::class, 'activateUser'])->name('user.activateUser');
    Route::get('restoreUser', [UserController::class, 'restoreUser'])->name('user.restoreUser');
    Route::get('getUpdateToAdmin/{id}', [UserController::class, 'getUpdateToAdmin'])->name('user.getUpdateToAdmin');
    Route::post('updateToAdmin', [UserController::class, 'updateToAdmin'])->name('user.updateToAdmin');
    Route::get('getChangePassword', [UserController::class, 'getChangePassword'])->name('user.getChangePassword');
    Route::get('changePassword/{id}', [UserController::class, 'changePassword'])->name('user.changePassword');
});
// ----------------------------------------Order---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'order'], function () {
    Route::post('update', [OrderController::class, 'update'])->name('order.update');
    Route::get('index', [OrderController::class, 'index'])->name('order.index');
    Route::get('edit/{id}', [OrderController::class, 'edit'])->name('order.edit');
    Route::get('show/{id}', [OrderController::class, 'show'])->name('order.show');
    Route::get('delete/{id}', [OrderController::class, 'destroy'])->name('order.destroy');
    Route::get('wait', [OrderController::class, 'ordersWait'])->name('order.wait');
    Route::get('done', [OrderController::class, 'ordersDone'])->name('order.done');
    Route::get('getOrderStudent/{id}', [OrderController::class, 'getOrderStudent'])->name('order.getOrderStudent');
    Route::get('getOrderTeacher/{id}', [OrderController::class, 'getOrderTeacher'])->name('order.getOrderTeacher');
});
// ----------------------------------------Exam---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'exam'], function () {
    Route::post('update', [ExamController::class, 'update'])->name('exam.update');
    Route::get('index', [ExamController::class, 'index'])->name('exam.index');
    Route::get('edit/{id}', [ExamController::class, 'edit'])->name('exam.edit');
    Route::get('show/{id}', [ExamController::class, 'show'])->name('exam.show');
    Route::get('delete/{id}', [ExamController::class, 'destroy'])->name('exam.destroy');
    Route::get('wait', [ExamController::class, 'examsWait'])->name('exam.wait');
    Route::get('done', [ExamController::class, 'examsDone'])->name('exam.done');
    Route::get('getExamStudent/{id}', [ExamController::class, 'getExamStudent'])->name('exam.getExamStudent');
    Route::get('getExamTeacher/{id}', [ExamController::class, 'getExamTeacher'])->name('exam.getExamTeacher');
});
// ----------------------------------------version---------------------------------------//
Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'ver'], function () {
    Route::get('index', [VersionController::class, 'index'])->name('version.index');
    Route::get('create', [VersionController::class, 'create'])->name('version.create');
    Route::post('store', [VersionController::class, 'store'])->name('version.store');
    Route::post('update', [VersionController::class, 'update'])->name('version.update');
    Route::get('edit/{id}', [VersionController::class, 'edit'])->name('version.edit');
    Route::get('delete/{id}', [VersionController::class, 'destroy'])->name('version.destroy');

});
