<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use HasFactory;
  //  use SoftDeletes;

    protected $table = 'exams';
    protected $appends = ['teacher_name','student_name','student_phone','teacher_phone'];
    protected $fillable = ['user_id', 'shapter_num', 'assigned_to', 'status', 'ejaza','reading',
        'rate', 'note', 'order_time', 'order_date', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStudentNameAttribute(){
        return $this->user()->pluck('name')->first();
    }
    public function getStudentPhoneAttribute(){
        return $this->user()->pluck('phone')->first();
    }

    public function assigned_to(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function getTeacherNameAttribute()
    {
        return $this->assigned_to()->pluck('name')->first();
    }
    public function getTeacherPhoneAttribute()
    {
        return $this->assigned_to()->pluck('phone')->first();
    }
}
