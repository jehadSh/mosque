<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    public function getAllowOrders_Exams(): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $user->allow_shapter = json_decode($user->allow_shapter, true);
        $orders = Order::where('assigned_to', null)->where('status', null)->where('user_id', '!=', $user->id)->orderBy('order_date', 'desc')->get();
        if ($user->isExam == 1) {
            $exams = Exam::where('assigned_to', null)->where('status', null)->where('user_id', '!=', $user->id)->orderBy('order_date', 'desc')->get();
        }
        else {
            $exams = Exam::where('order_date','[ih]')->get();
        }
        $response = [
            'orders' => $orders,
            'exams' => $exams
        ];
        if ($user->isEjaza) {
            return response()->json($response, 200);
        } else {

            for ($i = 0; $i < sizeof($orders); $i++) {

                if (!in_array($orders[$i]->shapter_num, $user->allow_shapter) || $orders[$i]->ejaza) {
                    unset($orders[$i]);
                }
            }
            for ($i = 0; $i < sizeof($exams); $i++) {
                if (!in_array($exams[$i]->shapter_num, $user->allow_shapter) || $exams[$i]->ejaza) {
                    unset($exams[$i]);
                }
            }
        }
        return response()->json($response, 200);
    }

    public function acceptOrder($id): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $order = Order::where('id', $id)->where('assigned_to', null)->where('status', null)->first();
        if ($order) {
            $order->update([
                'assigned_to' => $user->id,
                'status' => 'Processing'
            ]);
        } else {
            return response()->json('You have already accepted this Order', 404);
        }
        return response()->json($order, 201);
    }

    public function acceptExam($id): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $exam = Exam::where('id', $id)->where('assigned_to', null)->where('status', null)->first();
        if ($user->isExam == 0) {
            return response()->json('لا يحق لك اختبار أحد', 404);
        } else {
            if ($exam) {
                $exam->update([
                    'assigned_to' => $user->id,
                    'status' => 'Processing'
                ]);
            } else {
                return response()->json('You have already accepted this Exam', 404);
            }
            return response()->json($exam, 201);
        }
    }

    public function orderEvaluation(Request $request): \Illuminate\Http\JsonResponse
    {
        $order = Order::where('id', $request->id)->first();
        $order->update([
            'status' => $request->status,
            'note' => $request->note,
            'rate' => $request->rate,
        ]);
        return response()->json($order, 201);
    }

    public function examEvaluation(Request $request): \Illuminate\Http\JsonResponse
    {
        $exam = Exam::where('id', $request->id)->first();

        $exam->update([
            'status' => $request->status,
            'note' => $request->note,
            'rate' => $request->rate,
        ]);
        return response()->json($exam, 201);
    }

    public function getAllAccepted(): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $orders = Order::where('assigned_to', $user->id)->where(function ($query) {
            $query->where('status', null)
                ->orWhere('status', 'Processing');
        })->orderBy('order_date', 'desc')->get();
        $exams = Exam::where('assigned_to', $user->id)->where(function ($query) {
            $query->where('status', null)
                ->orWhere('status', 'Processing');
        })->orderBy('order_date', 'desc')->get();
        $response = [
            'orders' => $orders,
            'exams' => $exams
        ];
        if ($orders == null && $exams == null) {
            return response()->json('You Do not Have any Order or Exam', 404);
        } else {
            return response()->json($response, 200);
        }
    }

    public function getPreviousOrders_Exams(): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();

        $orders = Order::where('user_id', $user->id)->where(function ($query) {
            $query->where('status', 'Accepted')
                ->orWhere('status', 'Refused');
        })->orderBy('order_date', 'desc')->get();

        $exams = Exam::where('user_id', $user->id)->where(function ($query) {
            $query->where('status', 'Accepted')
                ->orWhere('status', 'Refused');
        })->orderBy('order_date', 'desc')->get();
        $response = [
            'orders' => $orders,
            'exams' => $exams
        ];

        // $orders = $orders->where('status',null)->orWhere('status','Processing')->first();
        if ($response) {
            return response()->json($response, 200);
        } else {
            return response()->json('You Do not Have any Order', 404);
        }
    }

    public function cancelAcceptOrder($id): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $order = Order::where('id', $id)->where('assigned_to', $user->id)->first();
        if ($order) {
            $order->update([
                'assigned_to' => null,
                'status' => null
            ]);
        } else {
            return response()->json('You not have already accepted this Order', 404);
        }
        return response()->json($order, 201);
    }

    public function cancelAcceptExam($id): \Illuminate\Http\JsonResponse
    {
        $user = Auth::user();
        $exam = Exam::where('id', $id)->where('assigned_to', $user->id)->first();
        if ($exam) {
            $exam->update([
                'assigned_to' => null,
                'status' => null
            ]);
        } else {
            return response()->json('You not have already accepted this Order', 404);
        }
        return response()->json($exam, 201);
    }
}
