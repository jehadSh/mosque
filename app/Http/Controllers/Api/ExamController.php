<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ExamController extends Controller
{
    public function index(Request $request)
    {
        $exam = Exam::where('id', '<>', 0);
        if ($request->with_trashed) {
            $exam = $exam->withTrashed();
        }
        $exam = $exam->get();
        return response()->json($exam, 200);
    }

    public function store_update(Request $request)
    {
        $user = Auth::user();
        if ($request->id) {
            $exam = Exam::where('id', $request->id)->first();
            $exam->update([
                'assigned_to' => null,
                'status' => null
            ]);
        } else {
            $check = Exam::where('user_id', $user->id)->where(function ($query) {
                $query->where('status', null)
                    ->orWhere('status', 'Processing');
            })->first();
            if ($check) {
                return response()->json('You already have a order.', 404);
            }
            $exam = new Exam();
            $exam->user_id = $user->id;
        }
        $exam->shapter_num = $request->shapter_num;
        $exam->note = $request->note;
        $exam->assigned_to = $request->assigned_to;
        $exam->rate = $request->rate;
        $exam->status = $request->status;
        $exam->reading = $request->reading;
        $exam->ejaza = $request->ejaza;
        $exam->order_date = $request->order_date;
        $exam->order_time = $request->order_time;
        $exam->save();
        return response()->json($exam, 201);
    }

    public function show($id)
    {
        //  $this->authorize('view',$skills);
        $exam = Exam::where('id', $id)->first();
        return response()->json($exam, 200);
    }

    public function destroy($id)
    {
        $exam = Exam::where('id', $id)->first();
        if ($exam) {
            $exam->delete();
            return response()->json($exam, 200);
        } else {
            $exam = Exam::withTrashed()->find($id)->restore();
            return response()->json($exam, 200);
        }
    }

    public function getMyExam()
    {
        $user = Auth::user();
        $exam = Exam::where('user_id', $user->id)->where(function ($query) {
            $query->where('status', null)
                ->orWhere('status', 'Processing');
        })->first();
        if ($exam) {
            return response()->json($exam, 200);
        } else {
            return response()->json('You Do not Have any Order', 404);
        }

    }

    public function getAllMyExam()
    {
        $user = Auth::user();
        $exams = Exam::where('user_id', $user->id)->get();
        if ($exams) {
            return response()->json($exams, 200);
        } else {
            return response()->json('You Do not Have any Order', 404);
        }
    }
}
