<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $order = Order::where('id', '<>', 0);
        if ($request->with_trashed) {
            $order = $order->withTrashed();
        }
        $order = $order->get();
        return response()->json($order, 200);
    }

    public function store_update(Request $request)
    {
        $user = Auth::user();
        if ($request->id) {
            $order = Order::where('id', $request->id)->first();
            $order->update([
                'assigned_to' => null,
                'status' => null
            ]);

        } else {
            $check = Order::where('user_id', $user->id)->where(function ($query) {
                $query->where('status', null)
                    ->orWhere('status', 'Processing');
            })->first();
            // return $check;                                                                                                                                                                            return $check;
            if ($check) {
                return response()->json('You already have a order.', 404);
            }
            $order = new Order();
            $order->user_id = $user->id;
        }
        $order->shapter_num = $request->shapter_num;
        $order->from_page = $request->from_page;
        $order->to_page = $request->to_page;
        $order->count_page = $request->to_page - $request->from_page + 1;
        $order->status = $request->status;
        $order->ejaza = $request->ejaza;
        $order->reading = $request->reading;
        $order->note = $request->note;
        $order->assigned_to = $request->assigned_to;
        $order->order_date = $request->order_date;
        $order->order_time = $request->order_time;
        $order->save();
        return response()->json($order, 200);

    }

    public function show($id)
    {
        //  $this->authorize('view',$skills);
        $order = Order::where('id', $id)->first();
        return response()->json($order, 200);
    }

    public function destroy($id)
    {
        $order = Order::where('id', $id)->first();
        if ($order) {
            $order->delete();
            return response()->json($order, 200);
        } else {
            $order = Order::withTrashed()->find($id)->restore();
            return response()->json($order, 200);
        }
    }

    public function getMyOrder()
    {
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)->where(function ($query) {
            $query->where('status', null)
                ->orWhere('status', 'Processing');
        })->first();

        // $orders = $orders->where('status',null)->orWhere('status','Processing')->first();
        if ($orders) {
            return response()->json($orders, 200);
        } else {
            return response()->json('You Do not Have any Order', 404);
        }
    }

    public function getAllMyOrder()
    {
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)->get();
        if ($orders) {
            return response()->json($orders, 200);
        } else {
            return response()->json('You Do not Have any Order', 404);
        }
    }
}
