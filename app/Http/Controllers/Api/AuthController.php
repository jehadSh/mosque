<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function register(UserRequest $request)
    {

        $user = User::create($request->all());
        $user->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $user->createToken('my-app-token')->plainTextToken;
        $response = ['user' => $user, 'token' => $token];
        return response()->json($response, 201);
    }

    public function login(Request $request)
    {
        if ($request->name == null) {
            return response()->json('pleas enter your name', 201);
        }
        $fields = $request->validate([
            'name' => 'string',
            'password' => 'required|string'
        ]);

        $user = User::where('name', $fields['name'])->first();
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'كلمة السر خاطئة'
            ], 401);
        }

        $token = $user->createToken('my-app-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

    public function logout(Request $request)
    {
        Auth::user()->tokens()->delete();
        return response()->json('Logged out', 200);
    }

    public function isToken(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            return response()->json($user, 200);
        } else {
            return response()->json('not login', 404);
        }

    }

    public function editPassword(Request $request){
        $user = Auth::user();
        $fields = $request->validate([
            'password' => 'required|string'
        ]);
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response(['message' => 'كلمة السر خاطئة'], 401);}
        $user->update([
            'password' =>  bcrypt($request->newPassword),

        ]);
        return response()->json('Done ', 200);
    }
}
