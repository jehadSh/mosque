<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function login(Request $request)
    {
        if ($request->name == null) {
            return response()->json('pleas enter your name', 201);
        }
        $fields = $request->validate([
            'name' => 'string',
            'password' => 'required|string'
        ]);

        $user = User::where('name', $fields['name'])->first();
        if ($user->role != 'admin') {
            return response([
                'message' => 'no allowed , you are not admin'
            ], 401);
        }
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad password'
            ], 401);
        }


        $token = $user->createToken('my-app-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

}
