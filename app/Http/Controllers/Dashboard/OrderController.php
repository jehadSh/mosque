<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('id', '<>', 0)->get();
        return view('orders.orders_index', compact("orders"));
    }

    public function update(Request $request)
    {
       // return $request;
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('id', $request->id)->get()->first();
        if ($request->status == 'null') {
           // return 'fsd';
            $orders->update([
                'status' => null
            ]);
        } else {
            $orders->update([
                'status' => $request->status,
            ]);
        }

        if($request->assigned_to){
            $nameTeacher = User::where('name', $request->assigned_to)->first();
            if ($nameTeacher->role != 'student') {
                $orders->update([
                    'assigned_to' => $nameTeacher->id,
                ]);
            }
        } else
        {
            $orders->update([
                'assigned_to' => null,
            ]);
        }

        $orders->update([
            'shapter_num' => $request->shapter_num,
            'from_page' => $request->from_page,
            'to_page' => $request->to_page,
            'count_page' => $request->to_page - $request->from_page + 1,
            'ejaza' => $request->ejaza,
            'reading' => $request->reading,
            'note' => $request->note,
            'rate' => $request->rate,
            'order_time' => $request->order_time,
            'order_date' => $request->order_date,
        ]);
        return redirect()->route('order.index');
    }

    public function edit($id)
    {
        $orders = Order::find($id);
        return view('orders.orders_edit', compact("orders"));
    }

    public function show($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('id', $id)->first();

        return view('orders.orders_show', compact("orders"));
    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('id', $id)->first();
        $orders->delete();
        return redirect()->route('order.index');
    }

    public function ordersWait(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('status',null)->orWhere('status','Processing')->get();
        return view('orders.orders_wait', compact("orders"));
    }

    public function ordersDone(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('status','Accepted')->orWhere('status','Refused')->get();
        return view('orders.orders_done', compact("orders"));
    }

    public function getOrderStudent($id){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('user_id',$id)->get();
        return view('orders.orders_index', compact("orders"));
    }
    public function getOrderTeacher($id){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $orders = Order::where('assigned_to',$id)->get();
        return view('orders.orders_index', compact("orders"));
    }
}
