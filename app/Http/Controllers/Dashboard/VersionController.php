<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Version;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VersionController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $versions = Version::where('id', '<>', 0)->get();
        return view('version.version_index', compact("versions"));
    }

    public function create()
    {
        return view('version.version_create');
    }

    public function store(Request $request)
    {
        $versions = new Version();
        $versions->link = $request->link;
        $versions->ver = $request->ver;
        $versions->save();
        return redirect()->route('version.index');
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $versions = Version::where('id', $request->id)->get()->first();
        $versions->update([
            'link' => $request->link,
            'ver' => $request->ver,
        ]);
        return redirect()->route('version.index');
    }

    public function edit($id)
    {
        $versions = Version::find($id);
        return view('version.version_edit', compact("versions"));
    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Version::where('id', $id)->first();
        $exams->delete();
        return redirect()->route('version.index');
    }


}
