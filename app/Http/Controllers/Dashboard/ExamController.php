<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('id', '<>', 0)->get();
        return view('exams.exams_index', compact("exams"));
    }

    public function update(Request $request)
    {

        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('id', $request->id)->get()->first();
        if ($request->status == 'null') {
            $exams->update([
                'status' => null
            ]);
        } else {
            $exams->update([
                'status' => $request->status,
            ]);
        }

        if($request->assigned_to){
            $nameTeacher = User::where('name', $request->assigned_to)->first();
            if ($nameTeacher->role != 'student') {
                $exams->update([
                    'assigned_to' => $nameTeacher->id,
                ]);
            }
        } else
        {
            $exams->update([
                'assigned_to' => null,
            ]);
        }

        $exams->update([
            'shapter_num' => $request->shapter_num,
            'ejaza' => $request->ejaza,
            'reading' => $request->reading,
            'note' => $request->note,
            'rate' => $request->rate,
            'order_time' => $request->order_time,
            'order_date' => $request->order_date,
        ]);
        return redirect()->route('exam.index');
    }

    public function edit($id)
    {
        $exams = Exam::find($id);
        return view('exams.exams_edit', compact("exams"));
    }

    public function show($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('id', $id)->first();
        return view('exams.exams_show', compact("exams"));

    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('id', $id)->first();
        $exams->delete();
        return redirect()->route('exam.index');
    }

    public function examsWait(){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('status',null)->orWhere('status','Processing')->get();
        return view('exams.exams_wait', compact("exams"));
    }
    public function examsDone(){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('status','Accepted')->orWhere('status','Refused')->get();
        return view('exams.exams_done', compact("exams"));
    }

    public function getExamStudent($id){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('user_id',$id)->get();
        return view('exams.exams_index', compact("exams"));
    }
    public function getExamTeacher($id){
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $exams = Exam::where('assigned_to',$id)->get();
        return view('exams.exams_index', compact("exams"));
    }
}
