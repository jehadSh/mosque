<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', '<>', 0);
        $user_models = $user_models->where('role', 'student')->get();
        return view('users.users_index', compact("user_models"));
    }

    public function create()
    {
        return view('users.users_create');
    }

    public function store(UserRequest $request)
    {
        $user_models = new User();
        $user_models->name = $request->name;
        $user_models->password = bcrypt($request->password);
        $user_models->class = $request->class;
        $user_models->phone = $request->phone;
        $user_models->group = $request->group;
        $user_models->role = $request->role;
        $user_models->activation = $request->activation;
        $user_models->allow_shapter = $request->allow_shapter;
        $user_models->isEjaza = $request->isEjaza;
        $user_models->isExam = $request->isExam;
        $user_models->save();
        return redirect()->route('user.index');
    }

    public function update(UserEditRequest $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $request->id)->get()->first();
        if ($request->allow_shapter) {
            $user_models->update([
                'allow_shapter' => $request->allow_shapter,
            ]);
        }

        $user_models->update([
            'name' => $request->name,
            'class' => $request->class,
            'phone' => $request->phone,
            'group' => $request->group,
            'role' => $request->role,
            'activation' => $request->activation,
            'isEjaza' => $request->isEjaza,
            'isExam' => $request->isExam,
        ]);
        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user_models = User::find($id);
        return view('users.users_edit', compact("user_models"));
    }

    public function show($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $id)->first();
        return view('users.users_show', compact('user_models'));
    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $id)->first();
        if ($user_models) {
            $user_models->delete();
            return redirect()->route('user.index');
        } else {
            User::withTrashed()->find($id)->restore();
            return redirect()->route('user.restoreUser');
        }
    }

    public function getAllTeachers()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('role', 'teacher')->orWhere('role', 'Listener')->orWhere('role', 'admin')->get();
        return view('users.teachers_index', compact("user_models"));
    }

    public function getViewActivateUser()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('activation', 0)->get();
        return view('users.users_activation', compact('user_models'));
    }

    public function activateUser($id)
    {
        $user = Auth::user();

        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $id)->first();
        $user_models->activation = 1;
        $user_models->save();
        return redirect()->route('user.getViewActivateUser');
    }

    public function restoreUser()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::withTrashed()->get();
        return view('users.users_delete', compact("user_models"));
    }

    public function getUpdateToAdmin($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $id)->first();
        return view('users.users_up_admin', compact('user_models'));
    }

    public function updateToAdmin(Request $request)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }

        $user_models = User::where('id', $request->id)->get()->first();
        $user_models->update([
            'email' => $request->email,
            'role' => $request->role,
        ]);
        return redirect()->route('user.index');
    }

    public function getChangePassword()
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', '<>', 0)->get();
        return view('users.users_password', compact("user_models"));
    }

    public function changePassword($id)
    {
        $user = Auth::user();
        if ($user->role != 'admin') {
            return response(['message' => 'no allowed , you are not admin'], 401);
        }
        $user_models = User::where('id', $id)->get()->first();

        $user_models->password = bcrypt("12345678");
        $user_models->save();
        return redirect()->route('user.index');

    }
}
